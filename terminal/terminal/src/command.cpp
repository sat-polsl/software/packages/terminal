#include "terminal/terminal.h"

namespace terminal {

command::command(std::string_view name, command_handler handler) : name_(name), handler_(handler) {
    detail::command_list::instance().push_back(this);
}

void command::handle(std::span<std::string_view> args) const { handler_(args); }

std::string_view command::name() const { return name_; }

detail::command_list& detail::command_list::instance() {
    static command_list storage;
    return storage;
}

void detail::command_list::push_back(command* command) {
    if (head_ == nullptr) {
        head_ = command;
        tail_ = command;
    } else {
        tail_->next_ = command;
        tail_ = command;
    }
}

detail::command_list::iterator detail::command_list::begin() {
    return command_list::iterator(head_);
}

detail::command_list::iterator detail::command_list::end() {
    return command_list::iterator(nullptr);
}

detail::command_list::iterator::iterator(command* pointer) : pointer_(pointer) {}

command& detail::command_list::iterator::operator*() const { return *pointer_; }

command* detail::command_list::iterator::operator->() { return pointer_; }

detail::command_list::iterator& detail::command_list::iterator::operator++() {
    pointer_ = pointer_->next_;
    return *this;
}

detail::command_list::iterator detail::command_list::iterator::operator++(int) {
    auto result = *this;
    pointer_ = pointer_->next_;
    return result;
}

} // namespace terminal
