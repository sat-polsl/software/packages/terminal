#pragma once
#include <algorithm>
#include <array>
#include <iterator>
#include <string_view>
#include <variant>
#include "etl/string.h"
#include "etl/vector.h"
#include "fmt/format.h"
#include "satext/expected.h"
#include "satext/inplace_function.h"
#include "satext/noncopyable.h"
#include "satos/chrono.h"
#include "spl/drivers/uart/concepts/uart.h"

namespace terminal {
/**
 * @defgroup terminal Terminal
 * @{
 */

class command;

namespace detail {
class command_list : private satext::noncopyable {
public:
    class iterator {
    public:
        using value_type = command;
        using difference_type = std::ptrdiff_t;
        using pointer = command*;
        using reference = command&;
        using iterator_category = std::forward_iterator_tag;

        iterator() = default;
        explicit iterator(command* pointer);

        bool operator==(iterator const& rhs) const = default;

        command& operator*() const;
        command* operator->();
        iterator& operator++();
        iterator operator++(int);

    private:
        command* pointer_{nullptr};
    };

    static command_list& instance();

    void push_back(command* command);

    iterator begin();
    iterator end();

private:
    command_list() = default;

    command* head_{nullptr};
    command* tail_{nullptr};
};
} // namespace detail

/**
 * @brief Command class
 * To add command define const static object of this class in global namespace
 * and pass name and callback function as constructor parameters
 */
class command : private satext::noncopyable {
public:
    friend detail::command_list;

    /**
     * @brief Command handler type
     */
    using command_handler = satext::inplace_function<void(std::span<std::string_view>), 4>;

    /**
     * @brief Constructor
     * @param name Command name
     * @param handler Command handler
     */
    command(std::string_view name, command_handler handler);

    /**
     * @brief Calls command with given arguments
     * @param args Command arguments
     */
    void handle(std::span<std::string_view> args) const;

    /**
     * Retruns command name
     * @return command name
     */
    std::string_view name() const;

private:
    std::string_view name_;
    command_handler handler_;
    command* next_{nullptr};
};

/**
 * @brief Terminal class.
 * @tparam UART UART driver.
 */
template<spl::drivers::uart::concepts::uart UART>
class terminal : private satext::noncopyable {
public:
    /**
     * @brief Constructor.
     * @param uart UART driver used by terminal.
     * @param buffer Buffer used by terminal.
     * @param args Arguments buffer.
     * @param prompt Terminal prompt.
     * @param welcome_message Welcome message printed after terminal initialization.
     */
    terminal(UART& uart,
             std::span<char> buffer,
             std::span<std::string_view> args,
             std::string_view prompt,
             std::string_view welcome_message = "") :
        uart_(uart),
        buffer_(buffer.data(), buffer.size()),
        args_(args.data(), args.size()),
        prompt_(prompt),
        welcome_message_(welcome_message) {
        std::ranges::fill(buffer_, 0);
        std::ranges::fill(args_, std::string_view{});
    }

    /**
     * Reads UART until line feed is received.
     * @param buffer buffer to save received data.
     * @param timeout timeout for reading.
     * @return buffer with received data.
     */
    satext::expected<std::span<const std::byte>, std::monostate>
    read_line(std::span<std::byte> buffer,
              satos::chrono::milliseconds timeout = satos::chrono::milliseconds::max()) {
        return uart_.read_until(buffer, next_line, timeout)
            .map([](auto result) {
                if (auto it = std::ranges::find(result, next_line); it != result.end()) {
                    return std::span(result.begin(), it);
                }
                return result;
            })
            .map_error([](auto) { return std::monostate{}; });
    }

    /**
     * @brief Prints to UART.
     * @tparam T arguments types.
     * @param format_string Format string complies with <a
     * href="https://fmt.dev/latest/syntax.html">{fmt}</a>
     * @param args Arguments
     */
    template<typename... T>
    void print(fmt::format_string<T...> format_string, T&&... args) {
        buffer_.clear();
        fmt::format_to(std::back_inserter(buffer_), format_string, std::forward<T>(args)...);
        auto to_send = std::span(buffer_.data(), buffer_.size());

        uart_.write(std::as_bytes(to_send));
    }

    /**
     * @brief Parses and calls given command
     * @param input input string view
     */
    void parse_command(std::string_view input) {
        if (input.empty()) {
            return;
        }

        std::size_t begin = 0;
        std::size_t end = 0;
        std::string_view cmd_name{};

        if (end = input.find(' ', begin); end != std::string_view::npos) {
            cmd_name = input.substr(begin, end);
            begin = input.find_first_not_of(' ', end);
            while (begin != std::string_view::npos && !args_.full()) {
                end = input.find_first_of(' ', begin);
                args_.push_back(input.substr(begin, end - begin));
                begin = input.find_first_not_of(' ', end);
            }
        } else {
            cmd_name = input;
        }

        auto& cmd_list = detail::command_list::instance();

        if (auto it = std::ranges::find(cmd_list, cmd_name, &command::name); it != cmd_list.end()) {
            it->handle(std::span(args_));
        } else {
            print("{} command not found\n", cmd_name);
        }
        args_.clear();
    }

    /**
     * @brief Terminal loop. Infinite loop of reading input and handling commands.
     */
    [[noreturn]] void loop() {
        print("\n\n{}\n", welcome_message_);
        for (;;) {
            print("{}", prompt_);
            buffer_.clear();
            read_line(std::as_writable_bytes(std::span(buffer_.data(), buffer_.capacity())))
                .map([this](auto received) {
                    parse_command(std::string_view(buffer_.data(), received.size()));
                });
        }
    }

private:
    UART& uart_;
    etl::string_ext buffer_;
    etl::vector_ext<std::string_view> args_;
    std::string_view prompt_;
    std::string_view welcome_message_;

    static constexpr std::byte next_line = std::byte{0xa};
};

/** @} */

} // namespace terminal
