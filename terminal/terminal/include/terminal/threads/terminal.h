#pragma once

#include <string_view>
#include "satext/units.h"
#include "satos/thread.h"
#include "terminal/terminal.h"

namespace terminal::threads {

template<typename T>
class terminal;

constexpr std::size_t thread_size = TERMINAL_THREAD_SIZE_KB * 1024;

template<typename T>
using terminal_base =
    satos::thread<terminal<T>, satos::thread_priority::normal_0, thread_size, "terminal">;

template<typename T>
class terminal : public terminal_base<T> {
    friend terminal_base<T>;

public:
    explicit terminal(::terminal::terminal<T>& terminal) : terminal_(terminal) {}

private:
    [[noreturn]] void thread_function() const { terminal_.loop(); }

    ::terminal::terminal<T>& terminal_;
};

} // namespace terminal::threads
