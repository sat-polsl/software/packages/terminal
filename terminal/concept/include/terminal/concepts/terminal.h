#pragma once
#include <concepts>
#include <cstddef>
#include <cstdint>
#include <span>
#include <string_view>
#include <variant>
#include "fmt/format.h"
#include "satext/expected.h"
#include "satos/chrono.h"

namespace terminal::concepts {

// clang-format off
template<typename T>
concept terminal = requires(T terminal, std::span<std::byte> buffer,
                            satos::chrono::milliseconds timeout, std::string_view sv,
                            fmt::format_string<std::uint32_t> format, std::uint32_t value) {
    { terminal.read_line(buffer, timeout) } -> std::same_as<satext::expected<std::span<const std::byte>, std::monostate>>;
    { terminal.parse_command(sv) } -> std::same_as<void>;
    { terminal.print(format, value) } -> std::same_as<void>;
    { terminal.loop() } -> std::same_as<void>;
};
// clang-format on
} // namespace terminal::concepts
