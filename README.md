[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sat-polsl_terminal&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=sat-polsl_terminal)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sat-polsl_terminal&metric=coverage)](https://sonarcloud.io/summary/new_code?id=sat-polsl_terminal)
[![Pipeline](https://gitlab.com/sat-polsl/software/packages/terminal/badges/main/pipeline.svg)](https://gitlab.com/sat-polsl/software/packages/terminal/-/pipelines?page=1&scope=all&ref=main)
[![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&query=%24%5B%3A1%5D.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F42010996%2Frepository%2Ftags)](https://gitlab.com/sat-polsl/software/packages/terminal/-/tags)

# Terminal

## Overview

This package provides terminal functionality with used-defined commands over UART.

### Supported targets:

- STM32L4 family

### Rules

1. Use conventional commits.
2. Update tag with version after merging to `main`.
3. Use `snake_case` for everything except template arguments which are named with `CamelCase`

### CMake options:

| Option                  | Default | Description                       |
|-------------------------|---------|-----------------------------------|
| TERMINAL_THREAD_SIZE_KB | "8"     | Size of terminal thread in KiB    |
| TERMINAL_BUFFER_SIZE    | "128"   | Size of terminal UART buffer      |
| TERMINAL_ARGUMENTS_SIZE | "10"    | Size of terminal arguments buffer |

## Usage:

### Creating terminal

Define `terminal::terminal` object and corresponding thread in application:

```c++
    using terminal_uart = decltype(drivers_.terminal_uart());
    terminal::terminal<terminal_uart> terminal_{drivers_.terminal_uart(), ">", "Welcome!"};
    terminal::threads::terminal<terminal_uart> terminal_thread_{terminal_};
```

### Defining commands
In any source file define `terminal::command` that takes command name as first argument and 
[Callable](https://en.cppreference.com/w/cpp/named_req/Callable) type with signature `void(std::span<std::string_view>)`

Example:
```c++
const terminal::command echo("echo", [](std::span<std::string_view> args) {
    for (std::size_t i = 0; i < args.size(); i++) {
        terminal().print("{}: {}\n", i, args[i]);
    }
});
```
