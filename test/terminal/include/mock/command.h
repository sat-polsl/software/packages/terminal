#pragma once

#include <span>
#include <string_view>
#include "gmock/gmock.h"

namespace mock {

struct command_mock {
    MOCK_METHOD(void, func_operator, (std::span<std::string_view>));
};

} // namespace mock
