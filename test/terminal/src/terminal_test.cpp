#include <array>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "etl/string.h"
#include "mock/command.h"
#include "spl/drivers/uart/mock/uart.h"
#include "terminal/concepts/terminal.h"
#include "terminal/terminal.h"

namespace {

using namespace ::testing;

using terminal_type = terminal::terminal<spl::drivers::uart::mock::uart>;
static_assert(terminal::concepts::terminal<terminal_type>);

struct TerminalTest : public Test {
    NiceMock<mock::command_mock> command_mock_;
    NiceMock<spl::drivers::uart::mock::uart> uart_;

    std::array<char, 128> buffer_;
    std::array<std::string_view, 10> args_;
};

TEST_F(TerminalTest, CommandCreated) {
    terminal::command test_command("test_command", [](std::span<std::string_view>) {});

    auto& command_list = terminal::detail::command_list::instance();
    ASSERT_THAT(&*command_list.begin(), Eq(&test_command));
    ASSERT_THAT(command_list.begin()->name(), Eq(test_command.name()));
}

TEST_F(TerminalTest, CommandInvoked) {
    terminal::command test_command("test_command", [this](std::span<std::string_view> args) {
        command_mock_.func_operator(args);
    });

    EXPECT_CALL(command_mock_, func_operator(_));

    terminal::detail::command_list::instance().begin()->handle({});
}

TEST_F(TerminalTest, ReadLineSuccess) {
    terminal_type terminal(uart_, buffer_, args_, ">");

    EXPECT_CALL(uart_, read_until_mock(_, std::byte{0x0a}, _))
        .WillOnce(Invoke([](auto buffer, auto, auto timeout) {
            EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds::max()));
            EXPECT_THAT(buffer.size(), Eq(127));
            buffer[0] = std::byte('t');
            buffer[1] = std::byte('e');
            buffer[2] = std::byte('s');
            buffer[3] = std::byte('t');
            buffer[4] = std::byte('\n');
            return buffer.subspan(0, 5);
        }));

    etl::string_ext buffer{buffer_.data(), buffer_.size()};
    auto result =
        terminal.read_line(std::as_writable_bytes(std::span(buffer.data(), buffer.capacity())));

    ASSERT_THAT(result.has_value(), Eq(true));
    auto readed = result.value();
    ASSERT_THAT(readed.size(), Eq(4));
    ASSERT_THAT(readed[0], Eq(std::byte(0x74)));
    ASSERT_THAT(readed[1], Eq(std::byte(0x65)));
    ASSERT_THAT(readed[2], Eq(std::byte(0x73)));
    ASSERT_THAT(readed[3], Eq(std::byte(0x74)));

    ASSERT_THAT(buffer_[0], Eq('t'));
    ASSERT_THAT(buffer_[1], Eq('e'));
    ASSERT_THAT(buffer_[2], Eq('s'));
    ASSERT_THAT(buffer_[3], Eq('t'));
    ASSERT_THAT(buffer_[4], Eq('\n'));
}

TEST_F(TerminalTest, ReadLineSuccessWithTimeout) {
    terminal_type terminal(uart_, buffer_, args_, ">");

    EXPECT_CALL(uart_, read_until_mock(_, std::byte{0x0a}, _))
        .WillOnce(Invoke([](auto buffer, auto, auto timeout) {
            EXPECT_THAT(timeout, Eq(satos::chrono::milliseconds(100)));
            EXPECT_THAT(buffer.size(), Eq(8));
            return satext::unexpected(spl::drivers::uart::status::timeout);
        }));

    std::array<std::byte, 8> buffer{};
    auto result = terminal.read_line(buffer, satos::chrono::milliseconds(100));

    ASSERT_THAT(result.has_value(), Eq(false));
}

TEST_F(TerminalTest, Print) {
    terminal_type terminal(uart_, buffer_, args_, ">");

    std::array<const std::byte, 8> expected{std::byte{0x44},
                                            std::byte{0x45},
                                            std::byte{0x41},
                                            std::byte{0x44},
                                            std::byte{0x43},
                                            std::byte{0x4F},
                                            std::byte{0x44},
                                            std::byte{0x45}};

    EXPECT_CALL(uart_, write_mock(_, _)).WillOnce(Invoke([expected](auto buffer, auto) {
        EXPECT_THAT(buffer.size(), Eq(expected.size()));
        for (std::size_t i = 0; i < buffer.size(); i++) {
            EXPECT_THAT(buffer[i], expected[i]);
        }
        return spl::drivers::uart::status::ok;
    }));

    terminal.print("DEAD{}", "CODE");
}

TEST_F(TerminalTest, ParseCommandEmpty) {
    terminal::command test_command("test_command", [this](std::span<std::string_view> args) {
        command_mock_.func_operator(args);
    });

    terminal_type terminal(uart_, buffer_, args_, ">");

    EXPECT_CALL(command_mock_, func_operator(_)).Times(0);

    terminal.parse_command("");
}

TEST_F(TerminalTest, ParseCommandSuccess) {
    terminal::command test_command("test_command", [this](std::span<std::string_view> args) {
        command_mock_.func_operator(args);
    });

    terminal_type terminal(uart_, buffer_, args_, ">");

    EXPECT_CALL(command_mock_, func_operator(_)).WillOnce(Invoke([](auto args) {
        EXPECT_THAT(args.size(), Eq(0));
    }));

    terminal.parse_command("test_command");
}

TEST_F(TerminalTest, ParsCommandSuccessWithArgs) {
    terminal::command test_command("test_command", [this](std::span<std::string_view> args) {
        command_mock_.func_operator(args);
    });

    terminal_type terminal(uart_, buffer_, args_, ">");

    EXPECT_CALL(command_mock_, func_operator(_)).WillOnce(Invoke([](auto args) {
        EXPECT_THAT(args.size(), Eq(2));
        EXPECT_THAT(args[0], Eq("arg1"));
        EXPECT_THAT(args[1], Eq("arg2"));
    }));

    terminal.parse_command("test_command arg1 arg2");
}

TEST_F(TerminalTest, ParseCommandNotFound) {
    terminal_type terminal(uart_, buffer_, args_, ">");

    EXPECT_CALL(uart_, write_mock(_, _)).WillOnce(Invoke([](auto buffer, auto) {
        EXPECT_THAT(buffer.size(), Eq(31));
        return spl::drivers::uart::status::ok;
    }));

    terminal.parse_command("test_command");
}

} // namespace
